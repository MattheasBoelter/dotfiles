set background=dark
hi clear
if exists("syntax_on")
	syntax reset
endif

let g:colors_name="waterfall"

" let g:colors = {
" 			\"black1": "#21292b",
" 			\"black2": "#273033",
" 			\"red1": "#b04646",
" 			\"red2": "#cc6666",
" 			\"green1": "#79af46",
" 			\"green2": "#a3dc6e",
" 			\"yellow1": "#c77a50",
" 			\"yellow2": "#f1cc84",
" 			\"blue1": "#3d6895",
" 			\"blue2": "#8fb0d2",
" 			\"magenta1": "#953d69",
" 			\"magenta2": "#c980a6",
" 			\"cyan1": "#3c9388",
" 			\"cyan2": "#8abeb7",
" 			\"white1": "#E6E4E0",
" 			\"white2": "#9E9D9A",
" 			\}

let s:wfc = {}

" Normal
let s:wfc.fg = ['#E6E4E0', 7]
let s:wfc.sub_fg = ['#9E9D9A', 7]
let s:wfc.bg = ['#21292b', 0]
let s:wfc.sub_bg = ['#273033', 0]

" Fundamental Highlights
let s:wfc.green = ['#a3dc6e', 2]
let s:wfc.blue = ['#72A3D7', 4]
let s:wfc.cyan = ['#8abeb7', 4]

" Secondary Highlights
let s:wfc.red = ['#b04646', 1]
let s:wfc.yellow = ['#f1cc84', 1]
let s:wfc.orange = ['#c77a50', 3]
let s:wfc.pink = ['#c980a6', 4]
let s:wfc.none = ['NONE', 'NONE']

function! s:HL(group, fg, bg, effects)
	execute 'hi ' . a:group . ' guifg=' . a:fg[0] . ' guibg=' . a:bg[0] . ' gui=' . a:effects
endfunction

" Editor
call s:HL('CursorLine', s:wfc.none, s:wfc.sub_bg, 'NONE')

call s:HL('LineNr', s:wfc.sub_fg, s:wfc.sub_bg, 'NONE')
call s:HL('CursorLineNr', s:wfc.blue, s:wfc.bg, 'NONE')
call s:HL('VertSplit', s:wfc.sub_bg, s:wfc.sub_bg, 'NONE')

call s:HL('CursorLineNr', s:wfc.blue, s:wfc.bg, 'NONE')

" Syntax
call s:HL('Normal', s:wfc.fg, s:wfc.none, 'NONE')
call s:HL('Identifier', s:wfc.cyan, s:wfc.none, 'NONE')
call s:HL('Function', s:wfc.orange, s:wfc.none, 'NONE')
call s:HL('Statement', s:wfc.green, s:wfc.none, 'NONE')
call s:HL('Comment', s:wfc.sub_fg, s:wfc.none, 'NONE')
call s:HL('String', s:wfc.pink, s:wfc.none, 'NONE')
call s:HL('Number', s:wfc.blue, s:wfc.none, 'NONE')
call s:HL('Special', s:wfc.cyan, s:wfc.none, 'NONE')
call s:HL('Type', s:wfc.cyan, s:wfc.none, 'NONE')

call s:HL('MatchParen', s:wfc.yellow, s:wfc.sub_bg, 'bold')

call s:HL('Todo', s:wfc.sub_fg, s:wfc.sub_bg, 'bold,underline')
call s:HL('Error', s:wfc.fg, s:wfc.red, 'bold')

" Search
call s:HL('Search', s:wfc.yellow, s:wfc.sub_bg, 'bold,underline')
call s:HL('IncSearch', s:wfc.yellow, s:wfc.sub_bg, 'bold,underline')

call s:HL('EndOfBuffer', s:wfc.bg, s:wfc.bg, 'NONE')

call s:HL('SpellBad', s:wfc.red, s:wfc.sub_bg, 'underline')
call s:HL('SpellCap', s:wfc.blue, s:wfc.sub_bg, 'underline')

" Git Gutter
call s:HL('SignColumn', s:wfc.none, s:wfc.sub_bg, 'NONE')
call s:HL('GitGutterAddDefault', s:wfc.green, s:wfc.sub_bg, 'NONE')
call s:HL('GitGutterChangeDefault', s:wfc.yellow, s:wfc.sub_bg, 'NONE')
call s:HL('GitGutterDeleteDefault', s:wfc.red, s:wfc.sub_bg, 'NONE')

call s:HL('StatusLine', s:wfc.fg, s:wfc.sub_bg, 'NONE')
call s:HL('StatusLineNC', s:wfc.sub_fg, s:wfc.bg, 'NONE')
" Actual Syntax
hi PreProc guifg=#a3dc6e guibg=NONE gui=NONE
" hi MatchParen guifg=#f1cc84 guibg=#273033 gui=bold
" hi Expression guifg=#21292b guibg=#79af46 gui=NONE
" hi Variable guifg=#b04646 guibg=NONE gui=NONE

set nocompatible

if empty(glob('~/.vim/autoload/plug.vim'))
	silent !curl -fLo ~/.config/nvim/autoload/plug.vim --create-dirs
		\ https://raw.githubusercontent.com/junegunn/vim-plug/master/plug.vim
	autocmd VimEnter * PlugInstall --sync | source $MYVIMRC
endif

"Plugins
call plug#begin()
Plug 'itchyny/lightline.vim' " Status Bar
Plug 'tpope/vim-surround' "Surrounding stuff
Plug 'tpope/vim-repeat' "Better repeating support

Plug 'airblade/vim-gitgutter' "Git diff in gutter. Similar to atom
Plug 'tomtom/tcomment_vim'
Plug 'Townk/vim-autoclose' "Bracket Autoclosing functionality

Plug 'pangloss/vim-javascript' "Javascript Highlighting. Dependency for JSX
Plug 'mxw/vim-jsx' "JSX Highlighting for react
Plug 'alvan/vim-closetag' "Nice tag closing
"Plug 'a-watson/vim-gdscript' "Godot Syntax Highlighting

Plug 'junegunn/goyo.vim'
Plug 'reedes/vim-pencil'
Plug 'plasticboy/vim-markdown'

Plug 'vimwiki/vimwiki'

call plug#end()

filetype plugin indent on
set backupcopy=yes "Apparently helps webpack with watching

set mouse= "Disable mouse

set nowrap
set tabstop=4
set backspace=indent,eol,start
set autoindent
set copyindent
set shiftwidth=4
set smarttab

set scrolloff=1

set path+=**
set wildmenu

set smartcase
set incsearch
set hlsearch

set number
set relativenumber

set splitbelow
set splitright

set ignorecase

setlocal fo+=aw

let g:closetag_filenames = "*.html,*.xhtml,*.phtml,*jsx"

au FocusGained,BufEnter * :silent! !

:autocmd InsertEnter,InsertLeave * set cul!
set gcr=a:blinkon0

set splitbelow
set splitright


set updatetime=50 "Update time for Git Gutter. Set super low cause I like responsiveness!

set termguicolors

set background=dark "for the dark version
set t_Co=256

syntax on

"colorscheme tender
colorscheme waterfall

"Custom Hotkeys

"Disble Arrow Keys
nnoremap <Left> <Nop>
nnoremap <Right> <Nop>
nnoremap <Up> <Nop>
nnoremap <Down> <Nop>

nmap <leader>ms <Plug>GitGutterStageHunk
nmap <leader>mu <Plug>GitGutterUndoHunk
nmap <leader>mp <Plug>GitGutterPreviewHunk
"Shift J and Shift K for faster navigation
nnoremap <S-j> 10j
nnoremap <S-k> 10k

"Set Leader to space
let mapleader = "\<SPACE>"

"Set semi-colon to be command line
nnoremap ; :
nnoremap : ;

"Open Splits with Leader u (vertical) i (horizontal)
nnoremap <leader>u :vsp<CR>
nnoremap <leader>i :sp<CR>

"Use Leader + hjkl to navigate windows
nnoremap <leader>h :wincmd h<CR>
nnoremap <leader>j :wincmd j<CR>
nnoremap <leader>k :wincmd k<CR>
nnoremap <leader>l :wincmd l<CR>

nnoremap <leader>v= :vertical res +10<CR>
nnoremap <leader>v- :vertical res -10<CR>

nnoremap <leader>b= :res +10<CR>
nnoremap <leader>b- :res -10<CR>
"Exit with leader q
nnoremap <leader>q :q<CR>

"Write file with Leader w, Open file with Leader e
nnoremap <leader>w :w<CR>
nnoremap <leader>e :e<SPACE>

"Move to left or right buffer
nnoremap <leader>f :bnext<CR>
nnoremap <leader>a :bprevious<CR>

"Next or Previous Issue
nnoremap <leader>d :lnext<CR>
nnoremap <leader>s :lprevious<CR>

nnoremap <leader>c gc

nnoremap <silent> <leader>p m`:let _s=@/ <Bar> :%s/\s\+$//e <Bar> :let @/=_s <Bar> :nohl <Bar> :unlet _s <CR>``

"Search for random string of characters to clear search highlighting.
nnoremap <silent> <leader>/ :nohlsearch<CR>

nnoremap <Up> :tabnew<CR>
nnoremap <Down> :tabclose<CR>
nnoremap <Left> :tabp<CR>
nnoremap <Right> :tabn<CR>

"Leader ; inserts ; at end of line
nnoremap <leader>; i<C-o>m`<C-o>A;<C-o>``<ESC>

"Leader , inserts , at end of line
nnoremap <leader>, i<C-o>m`<C-o>A,<C-o>``<ESC>

inoremap <expr><tab> pumvisible() ? "\<c-n>" : "\<tab>"

nnoremap <leader>z za

nnoremap <leader>g :Goyo<CR>

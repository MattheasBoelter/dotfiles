#!/bin/bash

#
# ~/.bashrc
#

# If not running interactively, don't do anything
[[ $- != *i* ]] && return

shopt -s autocd

alias c='calculator.sh'
alias n='nvim'

alias t='tmux'
alias tn='tmux new-session -t'
alias ta='tmux attach-session -t'
alias tk='tmux kill-session'

alias ls='ls -hN --color=auto --group-directories-first'
alias update='sudo pacman -Syu'

PS1='[\A][\W]\n$ '

PATH="$(ruby -e 'print Gem.user_dir')/bin:$PATH"

export EDITOR='nvim'

#
# ~/.bash_profile
#

XDG_CONFIG_HOME="$HOME/.config"
export XDG_CONFIG_HOME

export PATH=$PATH:/home/mattheas/.gem/ruby/2.6.0/bin

PATH=~/dotfiles/scripts:$PATH

[[ -f ~/.bashrc ]] && . ~/.bashrc

if [[ ! $DISPLAY && $XDG_VTNR -eq 1 ]]; then
	exec startx
fi

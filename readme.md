# Mattheas Boelter's Dotfiles

These are my dotfiles. Not everything is here quite yet, but they will be soon. It's all very messy, but I hopefully something will be useful.

## What is Here
This includes my bspwm config, my NeoVim config, and a couple shell scripts. This will be expanded. I use GNU Stow to place everything. To use it, simply type `stow [base dir] -t [target dir]`. The `base dir` is the top directory of a particular tree. In the case of NeoVim, that would be the `nvim` directory. The `target directory` is the directory where you want the links to appear. For Neovim again, this would be `~/.config/nvim/`.

If you have any questions or recommendations, please feel free to leave an issue and I'll try to help when I can.
